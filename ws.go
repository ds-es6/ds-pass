package main

import (
	"io/ioutil"
	"log"
	"mime"
	"net/http"
	"os"
	"time"

	"io"

	"github.com/gorilla/websocket"
)

type WSSMessage struct {
	Action int
	Cookie string
	Data   string
	Error  string
}

func NewWSSMessage() *WSSMessage {
	return &WSSMessage{
		Action: 0,
		Cookie: "",
		Data:   "",
		Error:  "",
	}
}

type WSSession struct {
	SessionId string
	Channel   chan *WSSMessage
}

func NewWSSession(sessionId string) *WSSession {
	return &WSSession{
		SessionId: sessionId,
		Channel:   make(chan *WSSMessage),
	}
}

type WSSessionManger struct {
	//SessionID cookie value
	Sessions map[*websocket.Conn]*WSSession
}

func NewWSSessionManager() *WSSessionManger {
	return &WSSessionManger{
		Sessions: make(map[*websocket.Conn]*WSSession),
	}
}

func (wss *WSSessionManger) Add(sessionId string, ws *websocket.Conn) *WSSession {
	session := NewWSSession(sessionId)
	wss.Sessions[ws] = session
	return session
}

func (wss *WSSessionManger) Remove(ws *websocket.Conn) {
	delete(wss.Sessions, ws)
}

func (wss *WSSessionManger) Send(sessionId string, data *WSSMessage) {
	for _, session := range wss.Sessions {
		if session.SessionId == sessionId {
			session.Channel <- data
		}
	}
}

//ReadClientWebsocket
func ReadClientWebsocket(ws *websocket.Conn, out chan *WSSMessage) {
	defer func() {
		log.Println("closed json.Receive relay")
		//send close to end connection
	}()
	log.Println("started json.Receive relay")
	for {
		msg := NewWSSMessage()
		err := ws.ReadJSON(&msg)

		if err != nil {
			//client is no longer available, closed connection
			if err == io.EOF {
				break
			}
			//malformed data client still connected
			//TODO: disconnect malious clients
			log.Println(err.Error())
			return
		}
		//foward to shared hub
		out <- msg
	}
}

type onConnnect struct {
	Action int
	Msg    string
}

//BroadcastWSS listens for WSMessage and sends JSON to client
func SharedHub(in chan *WSSMessage, ws *websocket.Conn) {
	defer func() {
		log.Println("closed json.Send replay")
	}()
	log.Println("started json.Send relay")
	for {
		//blocking
		select {
		case msg := <-in:
			switch msg.Action {
			//disconnected client
			case http.StatusGatewayTimeout:
				break
			//status 202
			case 200: //StatusOK
				err := ws.WriteJSON(msg)
				if err != nil {
					log.Println("unable to send websocket json: " + err.Error())
					continue
				}
			case 202: //StatusAccepted
				//send progress
				err := ws.WriteJSON(msg)
				if err != nil {
					log.Println("unable to send websocket json: " + err.Error())
					continue
				}
			//login/handshake
			case 100:
				//handle inputs from client
			default:
			}
		}
	}
}

var wsManager *WSSessionManger

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func handleWs(w http.ResponseWriter, r *http.Request) {

	ws, err := upgrader.Upgrade(w, r, nil) // error ignored for sake of simplicity
	if err != nil {
		log.Println(err)
		return
	}

	session := wsManager.Add("session", ws)

	//connection := NewWSSession()
	defer func() {
		log.Println("removing WS connection")
		wsManager.Remove(ws)
	}()

	ws.WriteJSON(onConnnect{
		Action: 10,
		Msg:    "hello world",
	})

	go SharedHub(session.Channel, ws)

	ReadClientWebsocket(ws, session.Channel)

	//ReadClientWebsocket closed clean up
	msg := NewWSSMessage()
	msg.Action = http.StatusGatewayTimeout
	session.Channel <- msg
	time.Sleep(time.Second * 1)
}

var srv http.Server

func handler(w http.ResponseWriter, req *http.Request) {
	enableCors(&w)
	f, err := os.Open("./static/dist/index.html")
	if err != nil {
		log.Panicln("unable to find dist/index.html")
	}
	b, _ := ioutil.ReadAll(f)
	f.Close()
	w.Write(b)
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func serveWSS() {

	var (
		srv http.Server
		mux = http.NewServeMux()
	)

	//mime.AddExtensionType(".ext", "media")

	mime.AddExtensionType("worker.js", "application/x-javascript")
	mux.HandleFunc("/ws", handleWs)
	mux.HandleFunc("/"+httpPath, handler)
	//mux.Handle("/static", http.StripPrefix("/static/", http.FileServer(http.Dir("/static"))))
	mux.Handle("/", http.FileServer(http.Dir("./static")))

	log.Println("started listening")

	srv = http.Server{
		Addr:    "localhost:" + port,
		Handler: mux,
		//WriteTimeout: WriteTimeoutFlag,
		//ReadTimeout: time.Second * 2,
		//IdleTimeout: time.Second * 2,
	}

	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("listen:%+s\n", err)
	}

}
