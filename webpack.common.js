const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: {
        polyfills: [
            'babel-polyfill',
        ],   
        main: './src/entry.js',
        style: ['./sass/main.scss'],
        devtool: 'source-map',
        //devtool: 'inline-source-map',    
    },

    

    //  we don't need TweenLight but webpack complains so added to externals.
    externals: {
        //TweenLite: 'TweenLite',
    },
    resolve: {
        alias: {
            //'ScrollMagic': path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js'),
            //'TweenLight': path.resolve('node_modules', 'greensock/src/TweenLight.js'),
            //'TweenMax': path.resolve('node_modules', 'greensock/src/TweenMax.js'),
            //'TimelineMax': path.resolve('node_modules', 'greensock/src/TimelineMax.js'),
            //'animation.gsap': path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js'),
            //'debug.addIndicators': path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js'),
        },
    },
    module: {
        rules: [
            {
                test: /\.s?[ac]ss$/,
                oneOf: [
                    {
                        resourceQuery: /inline/, // foo.css?inline
                        use: ['style-loader', 'css-loader', 'sass-loader'],
                    },                    
                    {
                        use: [
                            MiniCssExtractPlugin.loader,                            
                            'css-loader',
                            'sass-loader',
                        ],
                        
                    },
                ],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.worker\.js$/,
                use: { loader: "worker-loader" },
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader",
                        options: { minimize: false }
                    }
                ]
            }
        ]
    },
    plugins: [       
        new CleanWebpackPlugin(),
        
        new HtmlWebPackPlugin({
            template: './src/index.html',
            filename: "./index.html",
            minify:`false`,
            //inject:"body"
        }),
        
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: '[name].css',
//            filename: '[name].[chunkhash].css',
            chunkFilename: '[name].css',
        })
    ]
};
