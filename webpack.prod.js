const path = require('path');
const { merge } = require('webpack-merge')  
const Common = require('./webpack.common.js')

module.exports = merge(Common, {
  output: {
    filename: '[chunkhash].js',
    //filename: '[name].js',
    chunkFilename: '[chunkhash].bundle.js',
    path: path.resolve(__dirname, 'static','dist'),
    publicPath: '/dist/',
  },    
  mode:"production",    
  optimization: {
    usedExports: true,   
    runtimeChunk: 'single',
  },
  
  plugins: []
});