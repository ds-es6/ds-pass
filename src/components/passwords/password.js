import React, { Component } from 'react';


if (!window.decodepass){
  window.decodepass = (key, hash) =>{
    console.log("Webpack polyfill, golang injected function decode password:",key, hash)
    return hash
  }  
}

class Password extends Component {  

  constructor(props) {
    super(props);

    this.state = {
      username: props.data.Username,
      description: props.data.Description,
      password: props.data.Password,
      confirm: false,
      edit: false,
    }
  }

  async copyToClipboard(textToCopy) {
    // navigator clipboard api needs a secure context (https)
    if (navigator.clipboard && window.isSecureContext) {
        // navigator clipboard api method'
        return navigator.clipboard.writeText(textToCopy);
    } else {
        // text area method
        let textArea = document.createElement("textarea");
        textArea.value = textToCopy;
        // make the textarea out of viewport
        textArea.style.position = "fixed";
        textArea.style.left = "-999999px";
        textArea.style.top = "-999999px";
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        return new Promise((res, rej) => {
            // here the magic happens
            document.execCommand('copy') ? res() : rej();
            textArea.remove();
        });
    }
}

  copyClip(){
    
    var pass = decodepass(this.props.secret, this.state.password)

    /* Copy the text inside the text field */
    this.copyToClipboard(pass);

  }

  confirm(){
    this.setState({
      confirm: !this.state.confirm
    })
  }

  updateUser(e){
    this.setState({
      username: e.target.value
    })
  }
  updatePassword(e){
    this.setState({
      password: e.target.value
    })
  }
  updateRecord(e){
    this.props.update(e)
    this.setState({
      edit: false
    })
  }
  enableEdit(){
    this.setState({
      edit: true
    })
  }

  render() {

    var del = (
      <a onClick={this.confirm.bind(this)}>Delete</a>
    )

    if (this.state.confirm){
      del = (
        <a className="confirm" onClick={this.props.remove} 
        data-name={this.state.username}
        data-description={this.state.description}        
        >Confirm Delete</a>
      )
    }

    if (!this.state.edit){
      return (
        <div className="Block">
        
          <a onClick={this.enableEdit.bind(this)}>edit</a>
          <span>{this.state.description}</span> 
          <label>Username:
          <input type="text" value={this.state.username} readOnly disabled/>
          </label>
          <label>Password:
          <input type="password" value={this.state.password} readOnly disabled/>
          </label>
          <button onClick={this.copyClip.bind(this)}>copy</button>
        </div>
     );     
    }

    //edit view
    return (
      <div className="Block edit">
         {del}
         <form onSubmit={this.updateRecord.bind(this)}>
          <label>Description:
          <input type="text" value={this.state.description} readOnly/>
          </label>
          <label>Username:
          <input type="text" value={this.state.username} onChange={this.updateUser.bind(this)} />
          </label>
          <label>Password:
          <input type="text" value={this.state.password} onChange={this.updatePassword.bind(this)} />
          </label>
          <input type="submit" value="save"/>
        </form>
       
      </div>
   );       
  }
}

export default Password


