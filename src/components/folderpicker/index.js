require('./_folderpicker.scss?inline');

import React, { Component } from 'react';

class FolderPicker extends Component {  

  constructor(props) {
    super(props);
  }

  getFolder(event){

    console.log("GET FOLDER",event.target.files)

    var files = event.target.files;
    var path = files[0].webkitRelativePath;
    var Folder = path.split("/");
    console.log("FOLDER",Folder);
  }

  render() {
    return (    
      <React.Fragment>
        <input type="file" onChange={this.getFolder.bind(this)} webkitdirectory="true" mozdirectory="true" msdirectory="true" odirectory="true" directory multiple />
      </React.Fragment>     
    ); 
  }
}

export default FolderPicker


