require('./_app.scss?inline');

import React, { Component } from 'react';

import Passwords from '../passwords';
import Pin from '../pin';

import {APP_LOGIN, APP_VIEW, APP_OPTIONS, APP_ABOUT, APP_IMPORT} from '../../redux/slices/appSlice';

import {WebpackDevServerPolys} from "./poly"


WebpackDevServerPolys()

class App extends Component {  

  /*
    props.app.theme: dark
  */
  constructor(props) {  
    super(props);   
    this.state = {
      export: props.app.exportDir,
    }     
  }

  //create new user and skip login
  async createUser(event) {       
    event.preventDefault()
    var usr = event.target[0].value
    var pwd = event.target[1].value    
  
    var profilekey = await CreateProfile(usr,pwd)    
    var hash = await HashPassword(pwd)

     //store username
     window.localStorage.setItem("username", usr)
     window.localStorage.setItem("password", hash)

    this.props.saveUser({
      username: usr,
      password: pwd,
      key: profilekey
    })    
  }
  
  //login for found user account that has not been authenticated
  async loginUser(event) {    
    event.preventDefault()
    var usr = event.target[0].value
    var pwd = event.target[1].value    
    
    var localuser = window.localStorage.getItem("username")
    var hash = window.localStorage.getItem("password")

    var k = await GetKey(usr)
    var verify = await VerifyPassword(pwd, hash)
    if (usr != localuser || !verify){
        this.props.setError({
          error: "unable to login passwords do not match",
        })
        return
    }     
    this.props.loginUser({
      username: usr,
      password: pwd,
      key: k,
    })    
  }
 
  //import account username
  //file must be present in same directory
  async importUser(event) {    
    event.preventDefault()
    var usr = event.target[0].value
    var pwd = event.target[1].value    
    var error = await CheckUser(usr, pwd)  

    if (error !== ""){
      this.props.setError({
        error: "unable to import record: "+error,
      })
      return
    }   

    var profilekey = await GetKey(usr)
    var hash = await HashPassword(pwd)

    window.localStorage.setItem("username", usr)
    window.localStorage.setItem("password", hash)
 
    this.props.loginUser({
      username: usr,
      password: pwd,
      key: profilekey,
    })    
  }

  //update Password record
  async updatePassword(event) {    
    event.preventDefault()
   
    var des = event.target[0].value
    var usr = event.target[1].value
    var pwd = event.target[2].value    
    
    var record = {
      Username: usr,
      Description: des,
      Password: pwd
    }
   
    if (record.Username == ""
    || record.Description == ""
    || record.Password == ""
    ){
      this.props.setError({
        error: "unable to import record",
      })
      return
    }

    //update key file with items in app.
    var out = {
      User: this.props.app.user,
      Passwords: [],
    }

    for (var v of this.props.passwords.list){
      if(v.Description == record.Description){
        out.Passwords.push(record)
        continue
      }
      out.Passwords.push(v)
    } 
   
    await SavePasswords(
      this.props.app.user, 
      this.props.app.password, 
      JSON.stringify(out)
    )

    this.props.withPasswordList(out)      
  }

  async savePassword(event) {    
    event.preventDefault()
    var des = event.target[0].value
    var usr = event.target[1].value
    var pwd = event.target[2].value    
    
    var record = {
      Username: usr,
      Description: des,
      Password: pwd
    }

    if (record.Username == ""
    || record.Description == ""
    || record.Password == ""
    ){
      return
    }

    //update key file with items in app.
    var out = {
      User: this.props.app.user,
      Passwords: [],
    }   
    out.Passwords.push(...this.props.passwords.list) 
    out.Passwords.push(record)

    await SavePasswords(
      this.props.app.user, 
      this.props.app.password, 
      JSON.stringify(out)
    )

    this.props.savePassword(record)
    event.target[0].value = ""
    event.target[1].value = ""
    event.target[2].value = ""
  }

  async removePassword(event){
    
      var name = event.target.getAttribute("data-name")
      var des = event.target.getAttribute("data-description")

      //update key file with items in app.
      var out = {
        User: this.props.app.user,
        Passwords: [],
      }

      for (var item of this.props.passwords.list) {
        if (item.Description != des
        ){
          out.Passwords.push(item) 
        }
      }      
      
      await SavePasswords(
        this.props.app.user, 
        this.props.app.password, 
        JSON.stringify(out)
      )

      this.props.withPasswordList(out)

      

      
  }

  async loadPasswords() {    

    var passwords = await GetPasswords(
      this.props.app.user,
      this.props.app.password
    )
    
    try {
      var res = JSON.parse(passwords)
      this.props.withPasswordList(res)
    } catch (error) {
      console.log(error)      
    }
  }

  /**
   * Render either login page or Load the main app.
   */
  render() { 
   switch (this.props.app.view){
      case APP_VIEW:
        return (
          <div id="Application" className={this.props.app.theme}>   
              
              <Passwords 
              user={this.props.app.user}
              password={this.props.app.password}              
              updateList={this.loadPasswords.bind(this)}
              update={this.updatePassword.bind(this)}
              items={this.props.passwords.list}  
              remove={this.removePassword.bind(this)}            
              />

              <div className="NewPassword">
                
              
               <div className="Window addPassword">
               <h2>Add New Password</h2>
                <hr/>
                <form onSubmit={this.savePassword.bind(this)}>
                  <div>
                    <label>Description: <input type="text" /></label>
                  </div>
                  <div>
                    <label>Username: <input type="text" /></label>
                  </div>
                  <div>
                   <label>Password: <input type="password" /></label>
                  </div>
                  
                  
                  <input type="submit" value="Save" />
                </form>
                <hr/>
               </div>
               
              </div>    

          </div>  
        );
        case APP_OPTIONS:
          return (
            <div id="Application" className={this.state.theme}>             
            <div className="Window">
              <h1>OPTIONS</h1>
              <hr/>
              <input type="text" placeholder="export directory" value={this.props.app.exportDir} onChange={this.props.setExport} />
              <input type="text" placeholder="default list to display on boot" />
            </div>         
          </div> 
          );
        case APP_LOGIN:
            return (
              <div id="Application" className={this.props.app.theme}>   
                <form className="Login" onSubmit={this.loginUser.bind(this)}>
                  
                    <label>
                      User Account:<span>{this.props.app.user}</span>
                      <input type="hidden" value={this.props.app.user} disabled></input>
                    </label>
                    <label>
                        Pin
                      <Pin focus="true"/>
                    </label>
                    {this.props.app.error}
                    <hr/>
                   
                    <input type="submit" value="Unlock User"/>
                  
                </form>
              </div>    
            );
            case APP_IMPORT:
              return (
                <div id="Application" className={this.props.app.theme}>   
                  <div className="Window">
                  <h2>Import Account</h2>
                  <form className="Login" onSubmit={this.importUser.bind(this)}>
                  <label>
                      Username<br/>
                      <input type="text"></input>
                    </label>
                    <label>
                        Pin
                      <Pin focus="true"/>
                    </label>
                    {this.props.app.error}
                    <hr/>
                   
                    <input type="submit" value="Import User"/>
                  </form>
                
                  </div>
                </div>    
              );
            case APP_ABOUT:
            return (
              <div id="Application" className={this.props.app.theme}>   
                <div className="Window">
                  <h1>DS Pass</h1>
                  <h2>Secure Password Storage</h2>
                  <hr/>
                  <p>Secure Password storage</p>
                  <p>Encrypted Passwords stored @ username.pwfile in root directory</p>
                  <p>Profiles can be imported, file must be present with the same username</p>
                  <p>Do not loose the pin, you wont be able to recover files otherwise.</p>
                </div>
              </div>    
            );
        default:
          return (
            <div id="Application" className={this.props.app.theme}>   
                <div className="Window Login">
                  <h1>Create New User</h1>
                  <h2>Secure Password Storage</h2>

                  <form onSubmit={this.createUser.bind(this)}>
                    <label>
                      username<br/>
                    <input type="text" name="username" />
                    </label>
                    <hr/>
                    <label>
                          Pin
                        <Pin focus="true"/>
                      </label>
                      {this.props.app.error}
                      <hr/>
                      <p>Do not lose your pin, there is no way of recovering it.</p>
                  
                  <input type="submit" value="Create User"/>
                  </form>
                </div>
            </div> 
          );
    }
   
  }
}

export default App
