import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";

import { createStore } from "redux"
import allReducers from "./redux/reducer"


let store = createStore(allReducers)

//get state from local storage
//set app user if available
//defaults to new user view
//login if found
store.dispatch({ type: "app/setUser", payload: window.localStorage.getItem("username") })

import AppRedux from './redux/connect/app'

import(
    /* webpackChunkName: "menu" */
    './lib/menu'
).then(
(module) => {
    let LoadAppMenu = new module.default(store);      
});
   
document.oncontextmenu = function(e) {
    //e.preventDefault()
    //return false;
}

ReactDOM.render(
    <Provider store={store}>
        <AppRedux view="gallery" />
    </Provider>,
    document.getElementById("root")
);

