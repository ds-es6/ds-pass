function ToggleStatusbar(e){
    e.preventDefault();
    let sb = document.getElementById('status-bar');
    sb.classList.toggle('hide')
    sb.classList.toggle('enabled');    
    e.target.classList.toggle('checked')
}

function FullScreen(e){
    e.preventDefault();
    document.webkitRequestFullscreen();
}

function ReloadView(e){
    e.preventDefault();
    window.location.reload()
}


function ToggleSidebar(e){
    e.preventDefault();
    let activeSidebar = document.querySelector('.horizontal-resize')
    let s = {
        side: activeSidebar.querySelector('.side'),
        main:  activeSidebar.querySelector(".main"),
        shift: 0
    }
    if (s.side.classList.contains('hidden')){
        s.shift = 200       
    }
    s.side.setAttribute("style", "width:"+s.shift+"px;");
    s.main.setAttribute("style", "left:"+s.shift+"px;");
    s.side.classList.toggle('hidden');
    e.target.classList.toggle('checked')
}



export {ToggleSidebar,ReloadView, FullScreen, ToggleStatusbar}