import { ReloadView } from './events';

import {APP_NEW, APP_VIEW, APP_OPTIONS, APP_ABOUT, APP_IMPORT} from '../../redux/slices/appSlice';


const properties ={
    title: {
        container: "bars",
        menu: "menu"
    },
}

function MacElectronMenuTemplate(store){    
    console.log("STPRE",store)
    return [
        {
            label: "File",
            submenu: [{
              label: 'New User',         
              click () { 
                store.dispatch({ type: "app/handleView", payload: APP_NEW })
               }
            },
            {
                type:'separator'
            },
            {
                label: 'Open Profile',
                
                click () { 
                    store.dispatch({ type: "app/handleView", payload: APP_IMPORT })
                }
            },
            {
                type:'separator'
            },
            {
                label: 'Export Passwords',                
                click () { console.log('clicked file') }
            },
            {
                type:'separator'
            },
            {
                label: 'Preferences',                
                click () { 
                    store.dispatch({ type: "app/handleView", payload: APP_OPTIONS })
                 }
            },            
            {
                type:'separator'
            },
            {
                label: 'Exit',
                click (e) { 
                   quit() 
                } 
            }]
        },
        {
            label: "View",
            submenu: [{
             
              label: 'Import Profile',
              icon:"foo",            
              click (e, focusedWindow) {                     
                store.dispatch({ type: "app/handleView", payload: APP_IMPORT })
              }
            },
            {
                label: 'View Passwords',
                click (e) { 
                    //callback  
                    store.dispatch({ type: "app/handleView", payload: APP_VIEW })
                }            
              },
            {
             
                label: 'View Options',
                icon:"foo",
                role: 'TODO',
                type: 'checkbox',
                checked: true,
                click (e, focusedWindow) {                     
                  //window.localStorage.setItem("view","options");
                 store.dispatch({ type: "app/handleView", payload: "viewOptions" })

                }
              },
            {
                type:'separator'
            },
            {
              label: 'Reload',
              role: 'forceReload',
              click (e) { 
                ReloadView(e)  
              }                    
            }]
        },
        {
            label: "Help",
            submenu: [{
                label: 'About',
                click () { 
                    store.dispatch({ type: "app/handleView", payload: APP_ABOUT })
                 }
            }]
        }
    ];    
}

function NewMenu(store, MenuItems, title){
  
    let container = document.createElement('div');
    let menu = document.createElement('div');
    menu.classList = properties.title.menu;
    menu.setAttribute('data-title', title)
    container.classList = properties.title.container;
    menu.appendChild(MenuItems(store))
    container.appendChild(menu)
    return container
}

var clearmenu = (e) => {
    if (!e.target.matches( '.bars *')) {
        let menui = document.querySelectorAll('.menuitem')
        for(let m of menui){
            m.classList.remove('active');
        }    
        u.classList.remove('active');
        for (let t of u.children){
            if (typeof t.children[0] !== 'undefined'){
                t.children[0].classList.remove('active');                    
            }
        }  
    }              
}

function LinuxMenuItems(store){
    let u = LinuxFromTemplate(store),
       
    container = document.createElement('div')

 
    u.addEventListener('click', () => {
        u.classList.add('active');       
    });
   
    //disable menu
    document.addEventListener('click', (e) => {
        if (!e.target.matches( '.bars *')) {
            let menui = document.querySelectorAll('.menuitem')
            for(let m of menui){
                m.classList.remove('active');
            }    
            u.classList.remove('active');
            for (let t of u.children){
                if (typeof t.children[0] !== 'undefined'){
                    t.children[0].classList.remove('active');                    
                }
            }  
        }              
    });
    u.setAttribute("id","custom-menu-area")
    
     
    container.appendChild(u) 
    return container
}

function MacMenuItems(){
    let u = document.createElement('ul');
    u.appendChild(CloseControlBtn())
    u.appendChild(MinControlBtn())
    u.appendChild(MaxControlBtn())   
    return u;
}


//Create New Application menu item
function _NewMenuItem(icon, id, html, classList){
    let l = document.createElement('li');       
    if (icon){
        l.setAttribute('data-icon',icon)
    }
   
    if (id){
        l.setAttribute('id',id)
    }    
    if (html){
        l.innerHTML = html
    }    
    if (classList){
        l.classList = classList
    }    
    l.addEventListener('mouseover',() => {
        
        if (l.classList.contains('close')){
            l.classList.remove('close')
            return
        }

        let others = document.querySelectorAll('.menuitem')
        for (let o of others){
            o.classList.remove('active')       
        }
        l.classList.add('active');
            

    })    
    return l
}

function GenerateSubMenu(item){
    let sub = document.createElement('ul')
    for (let i in item.submenu){
        let subitem = item.submenu[i]
        let submenuitem = _NewMenuItem(subitem.icon,subitem.id, subitem.label, "menuitem")            

        if (subitem.type){
            console.log(subitem.type)
            if (subitem.type == "separator"){
                submenuitem.classList.add(subitem.type)            
            }
        }

        if (subitem.click){
            submenuitem.addEventListener("click", (e) => {
                let menui = document.querySelectorAll('.menu li')
                for(let m of menui){
                    console.log(m.classList)
                    m.classList.remove('active');
                }    
                subitem.click(e)
                sub.classList.add("close")
                setTimeout(()=>{
                    sub.classList.remove("close") 
                },1000)
            }
           )
        } 
        
        if (subitem.submenu){
            submenuitem.appendChild(
                GenerateSubMenu(submenuitem.submenu)
            )
        } 
        sub.appendChild(submenuitem)
    }    
    return sub
}

//Generate Linux/Windows Application menu from electron template
function LinuxFromTemplate(store){
    let u = document.createElement('ul'),
        template = MacElectronMenuTemplate(store)
    for (let item of template){    
    
        let sub = GenerateSubMenu(item)        
        let menuitem = _NewMenuItem(item.icon,item.id, item.label, "menuitem") 
        menuitem.appendChild(sub)
        u.appendChild(menuitem)
    }
    return u
}


export {NewMenu,LinuxMenuItems,MacMenuItems}
