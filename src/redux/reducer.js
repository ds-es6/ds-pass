import { combineReducers } from "redux"

//original style using seperated types/actions and reducer
//import classicReducer from "./classic/classicReducer"

//using new slice api
import {passwordSlice} from "./slices/password"
import {appSlice} from "./slices/appSlice"


const allReducers = combineReducers({
  passwords: passwordSlice.reducer,
  app: appSlice.reducer,
})

export default allReducers