import { createSlice } from '@reduxjs/toolkit'

export const APP_NEW = "viewNewProfile"
export const APP_LOGIN = "viewLogin"
export const APP_VIEW = "viewMain"
export const APP_OPTIONS = "viewOptions"
export const APP_IMPORT = "viewImport"
export const APP_ABOUT = "viewAbout"


export const appSlice = createSlice({  
    name: 'app',  
    initialState: {    
        view: "DEFAULT",
        theme: "dark",
        exportDir: "./",
        user: "",
        password: "",
        key: "",
        error:"",       
    },
    reducers: { 
        setExport: (state, action) => {
            state.exportDir = action.payload.target.value         
        }, 
        handleView: (state, action) => {
            state.view = action.payload          
        },
        setError: (state, action) => {
            state.error = action.payload.error
        },
        setUser: (state, action) => {
            console.log("SET USER")
            if (!action.payload){
                return
            }
            state.user = action.payload 
            state.view = APP_LOGIN         
        },

        loginUser: (state, action) => {
            
            
            state.user = action.payload.username 
            state.password = action.payload.password
            state.key = action.payload.key    
            state.view = APP_VIEW         
        }, 
        
        saveUser: (state, action) => {            
                   
            state.user = action.payload.username 
            //TODO: save hash of password/pin and store actual pin encoded in file
            //decode and check hash to verify
            state.password = action.payload.password                                   
            state.key = action.payload.key
            state.view = APP_VIEW 
        },        
    },
})
// Action creators are generated for each case reducer functionexport const { increment, decrement, incrementByAmount } = counterSlice.actions
export default appSlice.reducer