import { ADD_GAME } from "./types"

export const addGame = game => {
  return {
    type: ADD_GAME,
    payload: game,
  }
}

export const slice = game => {
    return {
      type: ADD_GAME,
      payload: game,
    }
  }