package main

type AccountPassword struct {
	Username    string
	Description string
	Password    string
}

type Passwords struct {
	User      string
	Passwords []*AccountPassword
}

func NewPswd() *Passwords {
	return &Passwords{
		User:      "",
		Passwords: make([]*AccountPassword, 0),
	}
}
