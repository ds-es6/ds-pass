module dspass

go 1.13

require (
	github.com/gorilla/websocket v1.4.2
	github.com/webview/webview v0.0.0-20210330151455-f540d88dde4e
	gitlab.com/ds-go/data v0.0.0-20210921021954-92fb2f3e0fa8
	gitlab.com/ds-go/skeleton v0.0.0-20210920225234-2c9ad6cd8e56
	gitlab.com/krobolt/go-data v0.0.0-20210821203546-dbc73edfb6b9
	golang.org/x/crypto v0.0.0-20210920023735-84f357641f63 // indirect
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
)
