package main

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"os"

	fs "gitlab.com/ds-go/data/fs"
)

const (
	headerLength = 512
	fileext      = ".pwfile"
	fileVersion  = byte(1)
)

//CreateFileHeader creates a formatted header for password file.
func CreateFileHeader(key string) []byte {
	k := []byte(key)
	//fill header length - key length with random chars
	h := []byte(fs.GenKey(headerLength - (len(k))))
	h[0] = fileVersion
	h[1] = byte(len(k))
	log.Println(len(h))
	h = append(h, k...)
	return h
}

//GetFileHeader returns key from header
func GetFileHeader(r io.Reader) string {
	var key []byte
	header := make([]byte, headerLength)
	r.Read(header)
	switch header[0] {
	case fileVersion:
		ln := int(header[1])
		key = header[headerLength-ln:]
	}
	return string(key)
}

//EncodePasswords to destination writer
func EncodePasswords(dst io.Writer, enc *fs.EncEncoder, pwd *Passwords) error {
	data, err := json.Marshal(pwd)
	if err != nil {
		return err
	}
	r := bytes.NewReader(data)
	//var b bytes.Buffer
	err = enc.Encode(r, dst)
	if err != nil {
		return err
	}
	return nil
}

//CreateNewPasswordFile creates a empty file
func CreateNewPasswordFile(user, pin string) (string, error) {
	var (
		pwds = NewPswd()
		e    = fs.NewEncoder()
	)

	//TODO: check for existence of file before creating
	pwds.User = user

	f, err := os.Create(user + fileext)
	if err != nil {
		return "", err
	}

	defer f.Close()

	key := fs.GenKey(60)
	e.WithKey(key + pin)

	f.Write(CreateFileHeader(key))
	err = EncodePasswords(f, e, pwds)
	if err != nil {
		return "", err
	}

	return key, nil
}

//UpdatePasswordFile replaces an existing password file, will fail if no file present.
func UpdatePasswordFile(user, pin, data string) error {

	var (
		pwds = NewPswd()
		e    = fs.NewEncoder()
		file = user + fileext
	)

	pwds.User = user

	err := json.Unmarshal([]byte(data), &pwds)
	if err != nil {
		return err
	}

	//check current file, should be able to decode
	key, _, err := ReadPasswordFile(user, pin)
	if err != nil {
		return err
	}

	e.WithKey(key + pin)

	//remove old record
	//TODO: make copy revert on failure
	os.Remove(file)
	f, err := os.Create(file)
	if err != nil {
		return err
	}

	//save header
	f.Write(CreateFileHeader(key))

	err = EncodePasswords(f, e, pwds)
	if err != nil {
		return err
	}

	return nil
}

//ReadPasswordFile decodes contents and returns key and file contents as a string
func ReadPasswordFile(user, pin string) (key string, data string, err error) {

	data = ""
	key = ""
	f, err := os.Open(user + fileext)
	if err != nil {
		return
	}

	key = GetFileHeader(f)
	e := fs.NewEncoder()
	e.WithKey(key + pin)

	var by bytes.Buffer
	err = e.Decode(f, &by)
	f.Close()
	if err != nil {
		return
	}

	data = by.String()
	return
}
