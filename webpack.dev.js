const path = require('path');
const { merge } = require('webpack-merge')  
const Common = require('./webpack.common.js')

const { WebpackPluginServe: Serve } = require('webpack-plugin-serve');

const host = "127.0.0.1"
const port = 7775


const options = { 
   host:host,
   open: true,
   hmr:true,
   liveReload:true,
   ramdisk:false,
   port:port,
   static: [path.resolve(__dirname, 'static','dist'), path.resolve('./static')]
   //static: {
  //  glob: [path.join(__dirname, 'static/**/dist')],
   // options: { onlyDirectories: true }
  //}
};

module.exports = merge(Common, {
    output: {
      filename: '[name].[chunkhash].js',
      //filename: '[name].js',
      chunkFilename: '[name].[chunkhash].bundle.js',
      path: path.resolve(__dirname, 'static','dist'),
      publicPath: 'http://'+host+":"+port+'/',
    },  
    mode:"development",
    watch:true,
    optimization: {
      usedExports: true,   
    },
    plugins: [       
        new Serve(options)
    ]
});