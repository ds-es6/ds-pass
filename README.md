# DS Pass

Secure Password Storage

![Secure Password Storage](https://gitlab.com/ds-es6/ds-pass/-/raw/main/docs/image.png)


Stores encrypted passwords to 'username.pwfile' 

.pwfile files can be imported but there is no way of recovering a lost pin.


Passwords are encoded against a 64 bit key (4 characters of which are your pin)

### .pwfile Header

account encrypt key consists of 60 random characters and the users pin.
The characters are stored in the first 512 bytes of the header. The pin
dictates the key position in the header.

e.g.
```
pin: 0010
```

###header:
```
V = file version = byte[0]
N = key length = byte[1]

k1 = start of key offset = byte[2] + 1st digit of pin = 2+pinDigit(0) = 2
k1 length = 1 + 2nd digit of pin = 1
k1 = byte[2:3]

k2 = end of k1 + 3rd digit of pin = byte[4]+pinDigit(1) = byte[5]
k2 len = 1 + 4th digit of pin = 1
k2 = byte[5:6]

k3 = remaining key length = N - len(k1) + len(k2)
k3 = last remaining bytes of header = byte[454:512]

key = k1+k2+k3+pin
key = byte[2:3] + byte[5:6] + byte[454:512] + []byte{0,0,1,0} 


```
