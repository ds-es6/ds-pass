/*
# Go-Data

Bind files to binary and access using file system.

Go Data compresses (GZip) and Encrypts (CFBEncrypt) files
and is accessible from its own filesystem. Files are stored
encrypted to the host filesystem and decrypts to memory on
file access.

Running Go-Data
Exporting files

Install go-data:

    go install gitlab.com/do-go/data

Running go-data

    ./go-data -import ./import-folder -export ./export-folder -o output.go

Exporting accepts the folling flags:


    -import: [./import-folder] the folder to encode
    -export: [./exported-folder] the folder to save encoded files to be referenced
    -o:      [output.go] the name of the go file to import into projects


Export creates the following files:

    ./exported-folder:  Contains encoded files to be included in project
    /output.go:         Go file to be included in project with Datastore.
    					output.go uses main namespace.

    user.key: generated key to be imported into project

Flags

    -import		Directory to import
    -export		Directory to export
    -o			Output go file
    -help		Go-Data help


Using Go-Data

Download go-data:

    go get gitlab.com/ds-go/go-data

Using Go-Data with a http.Fileserver

        package example

        import (
            "io/ioutil"
            "log"
            "net/http"
            "os"

            fs "gitlab.com/ds-go/data/fs"
        )

        func main() {
            //add custom ext so files are handled correctly
            //mime.AddExtensionType(ext, type)

            k, _ := os.Open("user.key")
            key, _ := ioutil.ReadAll(k)

            //Create new store with key from user.key
            d := fs.NewStore(string(key))

            //Populate store with user files
            //LoadGoData from output.go
            LoadGoData(d)

            gofs := goda.NewDataFileSystem(d)

            log.Fatal(http.ListenAndServe(":80", http.FileServer(gofs)))
        }


Using Go-Data in a project

        import (
            "html/template"
            "log"
            "net/http"

            fs "gitlab.com/ds-go/data/fs"
        )

        var Assets *fs.DataStore

        func Server() {
            handler := http.NewServeMux()
            handler.Handle("/whatever", fooHandler())
            http.ListenAndServe(":80", handler)
        }

        func fooHandler() http.Handler {
            fn := func(w http.ResponseWriter, r *http.Request) {

                //fetch contents
                f, err := Assets.GetContents("view/404.html")

                if err != nil {
                    log.Fatal(err)
                }
                tmp, err := template.New("template").Parse(string(f.Bytes))
                if err != nil {
                    log.Fatal(err)
                }
                tmp.Execute(w, nil)
            }
            return http.HandlerFunc(fn)
        }

*/
package fs
