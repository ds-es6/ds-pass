package fs

import (
	"compress/gzip"
	"io"
	"log"
)

func Zip(name string, dst io.Writer, src io.Reader) error {

	zw, err := gzip.NewWriterLevel(dst, gzip.BestCompression)
	if err != nil {
		return err
	}
	// Optional Header Fields
	zw.Name = name
	zw.Comment = GenKey(64)
	_, err = io.Copy(zw, src)

	if err != nil {
		return err
	}

	if err := zw.Close(); err != nil {
		return err
	}
	return nil
}

func Unzip(src io.Reader, dest io.Writer) error {
	zr, err := gzip.NewReader(src)
	if err != nil {
		log.Fatalln(err, "end")
		return err
	}
	if _, err := io.Copy(dest, zr); err != nil {
		return err
	}
	if err := zr.Close(); err != nil {
		return err
	}
	return nil
}
