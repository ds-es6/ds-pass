package fs

import (
	"os"
	"time"
)

//FileInfo implements os.File
type FileInfo struct {
	name        string // base name of the file
	size        int64  // length in bytes for regular files; system-dependent for others
	dir         bool
	modtime     time.Time
	mode        os.FileMode
	Bytes       []byte
	ContentType string
	Path        string
	Parent      string
	Tmp         *os.File
}

//NewFileInfo returns a new instance of FileInfo
func NewFileInfo() *FileInfo {
	return &FileInfo{
		name:        "",
		size:        0,
		dir:         false,
		modtime:     time.Now(),
		mode:        0,
		Bytes:       make([]byte, 0),
		ContentType: "",
		Path:        "",
		Parent:      "",
		Tmp:         nil,
	}
}

func CreateFileInfo(name string, size int64, mode os.FileMode, dir bool, modtime time.Time) *FileInfo {
	return &FileInfo{
		name:        name,
		size:        size,
		dir:         dir,
		modtime:     modtime,
		mode:        0,
		Bytes:       make([]byte, 0),
		ContentType: "",
		Path:        "",
		Parent:      "",
		Tmp:         nil,
	}
}

// Name of the file
func (dfi FileInfo) Name() string { return dfi.name }

//Size of file as length of bytes
func (dfi FileInfo) Size() int64 { return dfi.size }

// IsDir reports whether m describes a directory.
func (dfi FileInfo) IsDir() bool { return dfi.dir }

//// Sys is not used and always returns nil
func (dfi FileInfo) Sys() interface{} { return nil }

//// Mode returns os.Filemode inherted on import
func (dfi FileInfo) Mode() os.FileMode { return dfi.mode }

//// Mode returns Mod Time inherted on import
func (dfi FileInfo) ModTime() time.Time { return dfi.modtime }
